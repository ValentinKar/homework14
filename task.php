<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once('function.php'); 
require_once('connect_DB.php');   // соединение с базой данных

$pdo = Connect();    // соединение с базой данных
$task = 'task';     // название базы данных с мероприятиями

if ( isAction() && $_GET['action'] === 'delete' ) {     // удалить мероприятие
  $id = (integer)$_GET['id'];  
  $statement = $pdo->prepare("DELETE FROM $task WHERE id=?;");  
  $statement->execute( ["{$id}"] );  
  header('Location: task.php'); 
}; 

if ( isAction() && $_GET['action'] === 'done' ) {      // выполнить мероприятие
  $id = (integer)$_GET['id'];  
  $statement = $pdo->prepare("UPDATE $task SET is_done = 2 WHERE id = ?;"); 
  $statement->execute( ["{$id}"] );  
  header('Location: task.php'); 
}; 

if ( isNewOccasions() ) {       // добавляется новое мероприятие в базу данных
  $user = (integer)$_SESSION['id']; 
  $description = (string)$_POST['description'];  
  $assigned = (integer)$_POST['users']; 

  $statement = $pdo->prepare( "INSERT INTO $task (user_id, assigned_user_id, description, is_done, date_added)
  VALUES ( ?, ?, ?, 1, now() );" ); 
  $statement->execute( ["{$user}", "{$assigned}", "{$description}"] );  

    $statement = $pdo->query( "SELECT LAST_INSERT_ID();"); 
    foreach ($statement as $number) {    // вывожу id нового мероприятия
    echo "Добавлено мероприятие с id = " . $number['LAST_INSERT_ID()'] . "<br />"; 
    } 
}; 

$id = (integer) $_SESSION['id']; 
$statement = $pdo->prepare("SELECT id, login FROM user WHERE id LIKE ?;"); 
$statement->execute( ["{$id}"] );    // определяю login пользователя
    foreach ($statement as $row) { 
    $user_login = $row['login']; 
    }; 

$users = $pdo->prepare("SELECT * FROM user;"); // определяю список пользователей id, login для добавления мероприятия
$users->execute();

$task_autor = $pdo->prepare("SELECT      -- определяю список мероприятий назначенных пользователем и внесенных в таблицу task 
  t.id,            -- id мероприятия
  t.description,    -- описание мероприятия
  t.date_added,     -- дата добавления мероприятия
  t.is_done,           -- выполнено мероприятие или нет
  u1.login AS autors,   -- login автора мероприятия
  u2.login AS assigned,  -- login ответственного за мероприятие
  t.user_id AS id_autor  -- id автора мероприятия

   FROM $task AS t  
   LEFT JOIN user AS u1 ON u1.id=t.user_id 
   LEFT JOIN user AS u2 ON u2.id=t.assigned_user_id 
   -- GROUP BY id_autor 
   HAVING id_autor = ? 
   ;");     // определяю список мероприятий назначенных пользователем и внесенных в таблицу task 
$task_autor->execute( ["{$id}"] ); 


$task_user = $pdo->prepare("SELECT t.id,  -- вывожу список мероприятий за которые пользователь отвечает 
  t.description,      -- описание мероприятия
  t.date_added,       -- дата добавления мероприятия
  t.is_done,           -- выполнено мероприятие или нет
  u1.login AS autors,   -- login автора мероприятия
  u2.login AS assigned,   -- login ответственного за мероприятие
  t.assigned_user_id AS id_assigned  -- id ответственного за мероприятие

   FROM $task AS t  
   LEFT JOIN user AS u1 ON u1.id=t.user_id 
   LEFT JOIN user AS u2 ON u2.id=t.assigned_user_id 
   HAVING id_assigned = ? 
   ;");      // вывожу список мероприятий за которые пользователь отвечает 
$task_user->execute( ["{$id}"] ); 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>tasks</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
table {
border: 1px solid black; 
padding: 4px;
}
td {
border: 1px solid black; 
padding: 4px;
}
</style>
</head>
<body>

<h1>
Список мероприятий пользователя: 
<?php  echo $user_login;  ?>
</h1>

<form method="POST">
    <input type="text" name="description" placeholder="Описание задачи" value="" />
      <select size="1" name="users" >
        <option value="disabled" >чье мероприятие</option>
        <?php  foreach ($users as $user) :  ?>
        <option value='<?php echo $user["id"]; ?>'><?php echo $user["login"]; ?></option>
         <?php endforeach; ?>
      </select>
    <input type="submit" name="save" value="Добавить мероприятие" />
</form>

<br /><br />


<table>
    <tr>
        <th>Автор</th>
        <th>Ответственный</th>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
    </tr>
  <?php  foreach ( $task_autor as $row )  :  ?> 
  <tr>
    <td><?php  echo htmlspecialchars($row['autors']);  ?></td>
    <td><?php  echo htmlspecialchars($row['assigned']); ?></td>
    <td><?php echo htmlspecialchars($row['description']); ?></td>
    <td><?php  echo htmlspecialchars($row['date_added']);  ?></td>
    <td><?php switch( $row['is_done'] )  {
                case 1: 
                  echo 'не выполнено'; 
                  break;
                case 2: 
                  echo 'выполнено'; 
                  break; 
                default: 
                  echo 'странный статус'; 
                  break; 
              }; ?>
    </td>
    <td>
        <a href='edit.php?id=<?php echo $row['id']; ?>&action=edit'>Изменить или перепоручить</a> 
        <a href='?id=<?php echo $row['id']; ?>&action=done'>Выполнить</a> 
        <a href='?id=<?php echo $row['id']; ?>&action=delete'>Удалить</a> 
    </td>
  </tr>
  <?php  endforeach;  ?>
</table>

<br /><br />


<table>
  <tr>
    <th>Ответственный</th>
    <th>Автор</th>
    <th>Описание задачи</th>
    <th>Дата добавления</th>
    <th>Статус</th>
  </tr>
  <?php  foreach ( $task_user as $row )  :  ?> 
  <tr>
    <td><?php  echo htmlspecialchars($row['assigned']);  ?></td>
    <td><?php  echo htmlspecialchars($row['autors']); ?></td>
    <td><?php  echo htmlspecialchars($row['description']); ?></td>
    <td><?php  echo htmlspecialchars($row['date_added']);  ?></td>
    <td><?php switch( $row['is_done'] )  { 
                case 1: 
                  echo 'не выполнено'; 
                  break;
                case 2: 
                  echo 'выполнено'; 
                  break; 
                default: 
                  echo 'странный статус'; 
                  break; 
              }; ?> 
    </td>
  </tr>
  <?php  endforeach;  ?>
</table>

</body>
</html>