<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once ('connect_DB.php');   // соединение с базой данных

function isLogIndex()   // функция для авторизации пользователя на страницы index.php
{
    if ( isLogin() ) 
    {  
        if ( empty($_POST['password']) ) { 
        $_POST['password'] = '';        // если поле с паролем пустое
        }; 
        $statement = Connect()->prepare("SELECT * FROM user;"); 
        $statement->execute(); 

        foreach ( $statement as $user ) { 
            if ( (string)$_POST['login'] === $user['login'] && md5( (string)$_POST['password'] . getSalt() ) === $user['password'] ) 
            { 
            return $user['id']; 
            die; 
            }; 
        }; 

        return false;
    } 
    else 
    {
        return false;
    }
}; 

function isLogin()        // функция проверки ввел ли пользователь логин 
{
    return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST) && !empty($_POST['login']);
}; 

function getSalt() 
{
    return 'ibtfnwhcpq';
};