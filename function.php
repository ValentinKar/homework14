<?php 
error_reporting(E_ALL);        // вывести на экран все ошибки


function isAction()    // функция проверки, что надо вносить изменения в таблицу с мероприятиями
{ 
	return $_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['id']) && !empty($_GET['action']);
}; 


function isEdit()   // функция проверки изменяется ли мероприятие
{ 
	return $_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['id']) && $_GET['action'] === 'edit';
}; 


function isDescriptNew()   // функция проверки, передаются ли изменение мероприятия из формы
{ 
	return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['descript_new']) && !empty($_POST['id']); 
}; 


function isNewOccasions()    // функция проверки, добавляется ли новое мероприятие 
{ 
session_start(); 
	return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['description']) && $_POST['users'] !== "disabled" && !empty($_SESSION['id']); 
}; 