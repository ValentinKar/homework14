<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once 'login.php'; 

$isLogIndex = isLogIndex(); 
if ( !empty($isLogIndex) )  
{  
    session_start(); 
    $_SESSION['id'] = $isLogIndex;
    header('Location: task.php'); 
    die; 
} 
?> 
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Вход</title>
</head>
<body>

<h2>Авторизоваться</h2>

<form method="POST">

    <label for="login">Логин: </label>
    <input type="text" name="login" id="login">
    <br /><br />

    <label for="password">Пароль: </label>
    <input type="password" name="password" id="password">
    <br /><br />

    <button type="submit">Войти</button>
    <br />

</form>

<br />
<a href="registration.php">Зарегистрироваться</a>
</body>
</html>