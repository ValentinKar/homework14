<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once('login.php'); 
require_once('connect_DB.php'); 

if ( isLogin() )        // новый пользователь
{  
    $login = (string) $_POST['login'];
    $_POST['password'] = empty($_POST['password']) ? $_POST['password'] : ''; 
    $password = md5( (string)$_POST['password'] . getSalt() ); 

$pdo = Connect();  // соединяюсь с базой данных

$sth= $pdo->prepare("SELECT login FROM user WHERE login=:login;"); 
$sth->bindValue(':login', $login, PDO::PARAM_STR);
$sth->execute();
$result = $sth->fetch(PDO::FETCH_ASSOC);
if ( $result ) {    // проверяю уникальнось введеного login-а пользователя
    echo 'пользователь с именем ' . $result["login"] . ' уже есть в базе данных'; 
    die; 
}; 

$statement = $pdo->prepare( "INSERT INTO user (login, password) 
VALUES ( ?, ? );" );       // добавляю нового пользователя в таблицу базы данных
$statement->execute( ["{$login}", "{$password}"] );  


$statement = $pdo->lastInsertId(); 
session_start(); 
$_SESSION['id'] = $statement;
echo "Пользователь с номером id = " . $statement . " 
добавлен в базу данных 
<br /><br />
<a href=\"task.php\">Войти на сайт</a>
"; 
die; 
} 

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Регистрация нового пользователя</title>
</head>
<body>
<h2>Страница регистрации</h2>

<form method="POST">
    <label for="login">Логин: </label>
    <input type="text" name="login" id="login">
    <br /><br />

    <label for="password">Пароль: </label>
    <input type="password" name="password" id="password">
    <br /><br />

    <button type="submit">Регистрироваться</button>
</form>

</body>
</html>