<?php 
error_reporting(E_ALL);        // вывести на экран все ошибки
require_once('function.php'); 
require_once('connect_DB.php');   // соединение с базой данных


$pdo = Connect();    // соединение с базой данных
$task = 'task';      // название таблицы с мероприятиями в БД

if ( isDescriptNew() ) {       // изменить мероприятие
  $id = (integer)$_POST['id'];  
  $description = (string)$_POST['descript_new'];  
  $statement = $pdo->prepare("UPDATE $task SET description = ? WHERE id = ?;"); 
  $statement->execute( ["{$description}", "{$id}"] );  

  if ( $_POST['users'] !== 'disabled' ) {    // если изменен ответственный за мероприятие
    $id_user = (integer) $_POST['users']; 
    $statement = $pdo->prepare("UPDATE $task SET assigned_user_id = ? WHERE id = ?;"); 
    $statement->execute( ["{$id_user}", "{$id}"] );  
  }; 

header('Location: task.php'); 
}; 


$id = (integer) $_GET['id']; 

$user_login = $pdo->prepare("SELECT 
  t.id,              -- id мероприятия 
  u.login AS assigned  -- логин ответственного за мероприятие 
   FROM $task AS t  
   JOIN user AS u ON u.id=t.assigned_user_id 
   HAVING t.id = ? 
   ;"); 
$user_login->execute( ["{$id}"] ); 

if ( isEdit() )  {       // если мероприятие изменяется
  $statement = $pdo->prepare("SELECT id, description FROM task WHERE id LIKE ?;"); 
  $statement->execute( ["{$id}"] ); 
  $users = $pdo->prepare("SELECT id, login FROM user;"); 
  $users->execute(); 
}; 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Изменить мероприятие</title>
</head>
<body>

<h1>
Изменить мероприятие с id =  
<?php  echo htmlspecialchars( $id );  ?>
</h1>

Мероприятие было поручено: 
<?php  
foreach ($user_login as $row) { 
 echo htmlspecialchars($row['assigned']); 
}; 
?> 

<br /><br />

      <?php  
        if ( isEdit() )  :      // если мероприятие изменяется
        ?>
        <form method="POST">
          <input type="hidden" name="id" value="<?php echo htmlspecialchars($id); ?>" />

            <input type="text" name="descript_new" value="<?php     
              foreach ($statement as $row) { 
               echo htmlspecialchars($row['description']); 
             }; ?>" />

            <select size="1" name="users" >
              <option value="disabled" >Кому перепоручить?</option>
              <?php 
              foreach ($users as $user) :  ?>
              <option value='<?php echo $user["id"]; ?>'><?php echo htmlspecialchars($user["login"]); ?></option>
               <?php endforeach; ?>
            </select>

            <input type="submit" name="edit_descript" value="Редактировать мероприятие" />
        </form>

    <?php  endif;  ?>

</body>
</html>